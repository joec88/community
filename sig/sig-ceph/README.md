
# sig-ceph

- 基于LGPL version 2.1 or 3.0
- 基于Ceph项目丰富openeuler生态圈
- Ceph开源项目版本发布、更新以及维护



# 组织会议

北京时间，双周一上午，10点~11点


# 成员
MON:     mayilin,chenjian
OSD:     luorixin
RGW:     daizhiwei
MDS:     sunligang,wangxiaomeng
RADOS:   chixinze
RBD:     daizhiwei

### Maintainer列表

- chixinze[@chixinze](https://gitee.com/chixinze)
- liuzhiqiang[@liuzhiqiang26](https://gitee.com/liuzhiqiang26)



### Committer列表

- chixinze[@chixinze](https://gitee.com/chixinze)
- liuzhiqiang[@liuzhiqiang](https://gitee.com/liuzhiqiang)
- sunligang[@nick-slg-kylin](https://gitee.com/nick-slg-kylin)
- luorixin[@rosinL](https://gitee.com/rosinL)
- Zhiwei-Dai[@Zhiwei-Dai](https://gitee.com/Zhiwei-Dai)
- wangxiaomeng[@tjwangxm](https://gitee.com/tjwangxm)
- mayilin[@kymayl](https://gitee.com/kymayl)
- chenjian[@yahoohey](https://gitee.com/yahoohey)


# 联系方式


- 邮件列表 <dev@openeuler.org>



# 项目清单


项目名称：

repository地址：

- https:/gitee.com/src-openeuler/ceph
- https:/gitee.com/src-openeuler/ceph-deploy
- https:/gitee.com/src-openeuler/ceph-ansible
- https:/gitee.com/src-openeuler/ceph-csi
